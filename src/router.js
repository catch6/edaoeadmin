import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const Login = (resolve) => {
  import('common/Login').then((module) => {
    resolve(module)
  })
}

const Layout = (resolve) => {
  import('common/Layout').then((module) => {
    resolve(module)
  })
}

const Index = (resolve) => {
  import('page/Index').then((module) => {
    resolve(module)
  })
}

const Article = (resolve) => {
  import('page/Article').then((module) => {
    resolve(module)
  })
}

const ArticlePost = (resolve) => {
  import('page/ArticlePost').then((module) => {
    resolve(module)
  })
}

const Category = (resolve) => {
  import('page/Category').then((module) => {
    resolve(module)
  })
}

const Tag = (resolve) => {
  import('page/Tag').then((module) => {
    resolve(module)
  })
}

const Post = (resolve) => {
  import('page/Post').then((module) => {
    resolve(module)
  })
}

export default new Router({
  routes: [
    {path: '/', redirect: '/login'},
    {path: '/login', component: Login},
    {
      path: '/layout', component: Layout, children: [
      {path: 'index', component: Index},
      {path: 'article', component: Article},
      // {path: 'article/post', component: ArticlePost, meta: {keepAlive: true}},
      // {path: 'article/post/:articleId', component: ArticlePost, meta: {keepAlive: true}},
      {path: 'article/post', component: Post, meta: {keepAlive: true}},
      {path: 'article/post/:articleId', component: Post, meta: {keepAlive: true}},
      {path: 'category', component: Category},
      {path: 'tag', component: Tag},
    ]
    }
  ]
})
